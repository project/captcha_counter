
Wrong Captcha Counter Block


ABOUT
-----
The 'Wrong Captcha Counter Block' module provide block that display the 
amount of wrong CAPTCHA answears. Admin may see this count on the status 
page. This block is a similar to Akismet widget. Admin can set the text 
before and after this counter. Such widget so popular in the personal 
blogs on WordPress. You may even see such block in Akismet Blog 
(http://blog.akismet.com/). 


REQUIREMENTS
------------
Captcha of course.


DEMO
-----
The demo you may see in:
  * my blog (http://drupalblog.ru) (on Russian)
  * Akismet Blog (http://blog.akismet.com/)


PHP Support
-----------
PHP 5.3 supported.